// dekiru game center board data converter
// (c) Shaun "neko68k" Thompson 2017

/*
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <locale.h>
#include <string.h>
#include <libconfig.h>

typedef struct BRD_ENTRY {
	uint8_t name[0x22];
	uint8_t desc1[0x22];
	uint8_t desc2[0x22];
	uint8_t desc3[0x22];
	uint8_t stuff[0x0A];
}  ENTRY __attribute__ ((packed));

void nibswap(uint8_t* buf, uint8_t len){
	uint32_t count = 0;
	uint8_t thisByte = 0;
	for(count = 0; count<len; count++){
		thisByte = buf[count];
		buf[count] = (thisByte&0xF)<<4;
		buf[count] |= (thisByte&0xF0)>>4;
	}
}

int main(int argc, char *argv[]){
    FILE *inFile = NULL;
    FILE *outFile = NULL;
    char *ext = NULL;
    int c;

    char *inFN = NULL;
    char *outFN = NULL;

    if(argc<2){
        printf("Dekikon V1.0 - (C) 2017 Shaun \"neko68k\" Thompson\nUsage: dekikon <infile.BRD/CFG> <outfile.CFG/BRD>\n");
        return 1;
    }

    inFN = argv[1];
    outFN = argv[2];

    if(access( inFN, F_OK ) == -1){
        printf("Input file not found. Quitting.\n");
        return 2;
    }

    if(access( outFN, F_OK ) != -1){

        printf("Output file exists. Overwrite?.\ny/(n):");

        c = getchar();
        if(c == 'Y' || c == 'y')
            goto cont;

        printf("Quitting.\n");
        return 2;

    }

 cont:
    ext = strchr(inFN, '.');
    if(ext == NULL ){

        printf("Unknown input type. Quitting.\n");
        return 3;
    }

    if(!strcasecmp(ext, ".cfg")){
        outFile = fopen(outFN, "wb");
        inFile = fopen(inFN, "r");
        CFGtoBRD(inFile, outFile);
    }
    else if(!strcasecmp(ext, ".brd")){
        outFile = fopen(outFN, "wc");
        inFile = fopen(inFN, "rb");
        BRDtoCFG(inFile, outFile);
    }
    else {
        ext += 1;
        printf("Unknown input type '%s'. Quitting.\n", ext);
        return 4;
    }

    fclose(inFile);
    fclose(outFile);

    printf("Done.\n");
}

void CFGtoBRD(FILE *inFile, FILE *outFile){
    uint32_t num = 0;
    uint32_t numEntries = 0;
    char *inString = NULL;
    ENTRY entry;
    config_t cfgData;
	config_setting_t *root, *setting, *group, *array, *stuff = NULL;

	config_init(&cfgData);
	config_read(&cfgData, inFile);
	root = config_root_setting(&cfgData);

	array = config_lookup(&cfgData, "entries");

    for(numEntries = 0; numEntries < config_setting_length(array); numEntries++){
        group = config_setting_get_elem(array, numEntries);
        inString = (uint8_t*)config_setting_get_string(config_setting_get_member(group, "name"));
        strncpy(entry.name, inString, 0x22);
        nibswap(entry.name, 0x22);

        inString = (uint8_t*)config_setting_get_string(config_setting_get_member(group, "desc1"));
        strncpy(entry.desc1, inString, 0x22);
        nibswap(entry.desc1, 0x22);

        inString = (uint8_t*)config_setting_get_string(config_setting_get_member(group, "desc2"));
        strncpy(entry.desc2, inString, 0x22);
        nibswap(entry.desc2, 0x22);

        inString = (uint8_t*)config_setting_get_string(config_setting_get_member(group, "desc3"));
        strncpy(entry.desc3, inString, 0x22);
        nibswap(entry.desc3, 0x22);

        stuff =  config_setting_get_member(group, "stuff");

        for(num = 0; num<0xA;num++){
            entry.stuff[num] = (uint8_t)config_setting_get_int_elem(stuff, num);
        }

        nibswap(entry.stuff, 0xA);

        fwrite(&entry, 0x92, 1, outFile);
    }

    config_destroy(&cfgData);
}

void BRDtoCFG(FILE *inFile, FILE *outFile){
    uint8_t outByte = 0;

	uint32_t filesize = 0;
	uint32_t numEntries = 0;
	uint8_t	 block[0x90];
	uint32_t count = 0;
	uint8_t stuffByte = 0;

	ENTRY *entries = NULL;

	config_t cfgData;
	config_setting_t *root, *setting, *group, *array, *stuff;

	config_init(&cfgData);
	root = config_root_setting(&cfgData);

	fseek(inFile, 0, SEEK_END);
	filesize = ftell(inFile);
	fseek(inFile, 0, SEEK_SET);

	numEntries = filesize/0x92;

	entries = (ENTRY*)calloc(numEntries, 0x92);

	array = config_setting_add(root, "entries", CONFIG_TYPE_LIST);

	for(count = 0;count<numEntries;count++){

		fread(entries[count].name, 0x22, 1, inFile);
		fread(entries[count].desc1, 0x22, 1, inFile);
		fread(entries[count].desc2, 0x22, 1, inFile);
		fread(entries[count].desc3, 0x22, 1, inFile);

		nibswap(entries[count].name, 0x22);
		nibswap(entries[count].desc1, 0x22);
		nibswap(entries[count].desc2, 0x22);
		nibswap(entries[count].desc3, 0x22);

		fread(entries[count].stuff, 0x0A, 1, inFile);
		nibswap(entries[count].stuff, 0xA);

		group = config_setting_add(array, NULL, CONFIG_TYPE_GROUP);
        setting = config_setting_add(group, "name", CONFIG_TYPE_STRING);
        config_setting_set_string(setting, entries[count].name);
        setting = config_setting_add(group, "desc1", CONFIG_TYPE_STRING);
        config_setting_set_string(setting, entries[count].desc1);
        setting = config_setting_add(group, "desc2", CONFIG_TYPE_STRING);
        config_setting_set_string(setting, entries[count].desc2);
        setting = config_setting_add(group, "desc3", CONFIG_TYPE_STRING);
        config_setting_set_string(setting, entries[count].desc3);

        stuff = config_setting_add(group, "stuff", CONFIG_TYPE_ARRAY);

        for(stuffByte = 0; stuffByte<0xA; stuffByte++){
            setting = config_setting_add(stuff, NULL, CONFIG_TYPE_INT);
            config_setting_set_int(setting, entries[count].stuff[stuffByte]);
        }

	}

    free(entries);
    config_write(&cfgData, outFile);
    config_destroy(&cfgData);

	return 0;
}

